from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView


from . import views

urlpatterns = [
    path('login/', LoginView.as_view(template_name='kino/registration/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='kino/registration/logged_out.html'), name='logout'),
    # path('logout-then-login/', 'django.contrib.auth.views.logout_then_login', name='logout_then_login'),
    path('register/', views.register, name='register'),
    path('', views.dashboard, name='dashboard'),
    # path('login/', views.user_login, name='login'),
    path('home/', views.home, name='home'),
    path('page/', views.page, name='page'),
    path('list/', views.list, name='list'),
    path('list/add', views.add, name='add'),
    path('<int:film_id>', views.detail, name='detail'),
    path('list/delet', views.delet, name='delet'),
]
