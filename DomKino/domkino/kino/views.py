from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import Film
from .forms import filmForm, LoginForm, UserRegistrationForm


@login_required
def dashboard(request):
    return render(request, 'kino/dashboard.html', {'section': 'dashboard'})


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'kino/login.html', {'form': form})


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            # profile =
            return render(request, 'kino/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'kino/register.html', {'user_form': user_form})


def home(request):
    return HttpResponse('<h2> Это домашняя страница </h2>')


def page(request):
    return render(request, 'kino/page.html')


def list(request):
    list=Film.objects.all()
    return render(request, 'kino/list.html', context={'pr_list': list})


def detail(request, film_id):
    try:
        p = Film.objects.get(id=film_id)
    except:
        raise Http404("Информации об этом фильме нет!")
    return render(request, 'kino/detail.html', {'product': p})


def delet(request):
    if request.method == "POST":
        try:
            films_id = request.POST.getlist('to_remove')
            if len(films_id) == 1:
                film = Film.objects.get(id=films_id[0])
                film.delete()
                return redirect('list')
            elif len(films_id) > 1:
                for film_id in range(len(films_id)):
                    film = Film.objects.get(id=films_id[film_id])
                    film.delete()
                return redirect('list')
            else:
                return redirect('list')
        except:
            raise Http404("Не фортануло")
    else:
        return render(request, 'kino/delet.html')


def add(request):
   if request.method == "POST":
        form = filmForm(request.POST)
        if form.is_valid():
            Film.objects.create(**form.cleaned_data)
            return redirect('list')
   else:
        form = filmForm()
        return render(request, 'kino/addprod.html', {'form': form})

