from django import forms
from django.contrib.auth.models import User

from .models import Profile, Film


class filmForm(forms.Form):
    f_name = forms.CharField(max_length=250, label="Наименование",
                         widget=forms.TextInput(attrs={'class':'mdl-textfield__input'}))
    f_date = forms.DateTimeField(label="Дата выхода")
    f_rate = forms.IntegerField(label="Рейтинг")
    f_genre = forms.CharField(max_length=250, label="Жанры",
                           widget=forms.TextInput(attrs={'class': 'mdl-textfield__input'}))
    f_description = forms.CharField(label="Описание",
                         widget=forms.Textarea(attrs={'class': 'mdl-textfield__input'}))


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Пароли не совпадают.')
        return cd['password2']


