from django.db import models
from django.conf import settings

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, primary_key = True)
    date_of_birth = models.DateField(blank=True, null=True)

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)


class Film(models.Model):
    name = models.CharField('Название фильма', max_length=250)
    description = models.TextField('Описание фильма')
    date = models.DateTimeField('Дата выхода')
    rate = models.IntegerField('Рейтинг')
    genre = models.CharField('Жанры', max_length=250)

    def __str__(self):
        return self.name