from django.contrib import admin
from .models import Film, Profile

# Register your models here.
admin.site.register(Film)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'date_of_birth']


admin.site.register(Profile, ProfileAdmin)

